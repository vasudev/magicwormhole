package magicwormhole

import (
	"fmt"
	message "salsa.debian.org/vasudev/magicwormhole/internal/servermessages"
)

// ServerError is returned when error message is received by server and contains
// content of error message and the original message which caused the error.
type ServerError struct {
	msg  string
	orig message.OutboundMessage
}

func (s *ServerError) Error() string {
	return fmt.Sprintf("Got error message from server: %s, for: %v", s.msg, s.orig)
}
