package magicwormhole

import (
	"crypto/rand"
	"encoding/hex"
)

func generateSide() (string, error) {
	side := make([]byte, 5)
	_, err := rand.Read(side)
	if err != nil {
		return "", err
	}

	return hex.EncodeToString(side), nil
}
