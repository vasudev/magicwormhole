package core

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
	"net/url"
	message "salsa.debian.org/vasudev/magicwormhole/internal/servermessages"
)

// Session represents connection state to Rendezvous server.
type Session struct {
	conn         *websocket.Conn
	ToApp        chan message.InboundMessage
	ForServer    chan message.OutboundMessage
	stopReceiver chan struct{}
	stopSender   chan struct{}
}

// NewSession opens a new connection to rendezvous server with given url and
// returns a control session object.
func NewSession(u string) (*Session, error) {
	server, err := url.Parse(u)
	if err != nil {
		return nil, err
	}

	conn, _, err := websocket.DefaultDialer.Dial(server.String(), nil)
	if err != nil {
		return nil, err
	}

	var session Session
	session.conn = conn
	session.ToApp = make(chan message.InboundMessage)
	session.ForServer = make(chan message.OutboundMessage)
	session.stopReceiver = make(chan struct{})
	session.stopSender = make(chan struct{})

	go receiver(session.conn, session.ToApp, session.stopReceiver)
	go sender(session.conn, session.ForServer, session.stopSender)

	return &session, nil
}

// Close closes the current active session with rendezvous server.
func (session *Session) Close() error {
	session.stopSender <- struct{}{}
	session.stopReceiver <- struct{}{}

	return session.Close()
}

// This function is periodically reading from the server and send the recevied
// message up to the application.
func receiver(conn *websocket.Conn, sendTo chan<- message.InboundMessage, quit chan struct{}) {
	for {
		// We check if we are signaled to stop from application in that
		// case we return from the go routine. Else we will block on read.
		select {
		case <-quit:
			return
		default:
			// TODO: How do we notify main goroutine  that
			// have errored out?.
			_, msg, err := conn.ReadMessage()
			if err != nil {
				if websocket.IsUnexpectedCloseError(err,
					websocket.CloseGoingAway,
					websocket.CloseAbnormalClosure) {
					log.Printf("error: %s", err)
				}
				break
			}

			toapp, err := message.Decode(json.RawMessage(msg))
			if err != nil {
				log.Printf("Error decoding: %s", err)
				break
			}

			sendTo <- toapp
		}
	}
}

// This function is receiving message from application and sending it to server.
func sender(conn *websocket.Conn, recvFrom <-chan message.OutboundMessage, quit chan struct{}) {
	for {
		select {
		case message, ok := <-recvFrom:
			if !ok {
				// Application closed the channel
				conn.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}

			w, err := conn.NextWriter(websocket.TextMessage)
			if err != nil {
				return
			}

			msgbytes, err := json.Marshal(message)
			if err != nil {
				log.Printf("Error marshaling message: %v, %s", message, err)
				continue
			}

			_, err = w.Write(msgbytes)
			if err != nil {
				log.Printf("Error transfering message: %v, %s", message, err)
				return
			}

		case <-quit:
			return
		}
	}
}
