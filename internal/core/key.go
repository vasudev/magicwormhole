package core

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"golang.org/x/crypto/hkdf"
	"golang.org/x/crypto/nacl/secretbox"
	"io"
	spake2 "salsa.debian.org/vasudev/gospake2"
	peer "salsa.debian.org/vasudev/magicwormhole/internal/peermessages"
)

const (
	keySize   = 32
	nonceSize = 24
)

// EncryptData encrypts given `plaintext` using `key` and utilizing secretbox
// from nacl library and returns encrypted message. If key size is not equal to
// 32 bytes or if there were error in generating nonce an error is returned.
func EncryptData(keyBytes, plaintext []byte) ([]byte, error) {
	if len(keyBytes) != keySize {
		return nil, fmt.Errorf("key should be of %d byte size but its %d bytes", keySize, len(keyBytes))
	}

	var nonce [nonceSize]byte
	var key [keySize]byte

	copy(key[:], keyBytes)

	_, err := rand.Read(nonce[:])

	if err != nil {
		return nil, err
	}

	return secretbox.Seal(nonce[:], plaintext, &nonce, &key), nil
}

// DecryptData accepts encrypted returned from EncryptData and the key and uses
// secretbox to decrypt the data and return plain text bytes. If key size does
// not match 32 bytes or if the decryption failed an error is returned.
func DecryptData(keyBytes, encrypted []byte) ([]byte, error) {
	if len(keyBytes) != keySize {
		return nil, fmt.Errorf("key size does not match %d bytes", keySize)
	}

	var nonce [nonceSize]byte
	var key [keySize]byte

	copy(key[:], keyBytes)
	copy(nonce[:], encrypted[:nonceSize])

	decrypted, ok := secretbox.Open(nil, encrypted[nonceSize:], &nonce,
		&key)
	if !ok {
		return nil, fmt.Errorf("decryption failed")
	}

	return decrypted, nil
}

func deriveKey(key, purpose []byte) []byte {
	hkdfReader := hkdf.New(sha256.New, key, nil, purpose)
	k := make([]byte, keySize)
	io.ReadFull(hkdfReader, k)
	return k
}

// DerivePhaseKey allows to derive a new key based on side and phase of
// sender/receiver.
func DerivePhaseKey(key []byte, side, phase string) []byte {
	purpose := []byte("wormhole:phase:")
	sideHash := sha256.Sum256([]byte(side))
	phaseHash := sha256.Sum256([]byte(phase))
	purpose = append(purpose, sideHash[:]...)
	purpose = append(purpose, phaseHash[:]...)
	return deriveKey(key, purpose)
}

// PhaseMessage represents container used to transport SPAKE2 exchange value to
// other side.
type PhaseMessage struct {
	Pakev1 string `json:"pake_v1"`
}

// Key holds internal state for Key generation and encryption in wormhole.
type Key struct {
	appid, side string
	version     map[string]string
	code        *string
	spakeState  spake2.SPAKE2
}

const (
	invalidPake = iota << 1
	keyDerivationFail
)

// Scared is error generated when invalid pake message is obtained or SPAKE2
// calculation fails.
type Scared struct {
	kind int
	err  error
}

func (s Scared) Error() string {
	switch s.kind {
	case invalidPake:
		return fmt.Sprintf("Invalid PAKE message recieved: %s", s.err)
	case keyDerivationFail:
		return fmt.Sprintf("Key derivation failed: %s", s.err)
	}

	// Should not be reached
	return ""
}

// NewKey creates key machine for wormhole with given appid, side and version
// string.
func NewKey(appid, side string, version map[string]string) Key {
	return Key{appid: appid, side: side, version: version}
}

// BuildPake should be called when wormhole is allocated with a password
// internally using PGP wordlist or when user supplies password. It generates
// pake message to be sent to other party
func (k *Key) BuildPake(code string) ([]byte, error) {
	// Store code into internal state
	// TODO: Do we really need to check this?
	if k.code == nil {
		k.code = new(string)
	}

	*k.code = code

	passcode := spake2.NewPassword(code)
	identity := spake2.NewIdentityS(k.appid)
	k.spakeState = spake2.SPAKE2Symmetric(passcode, identity)
	msg := k.spakeState.Start()
	body := PhaseMessage{Pakev1: hex.EncodeToString(msg)}
	return json.Marshal(body)
}

// ComputeKey should be called when pake message is received from other side.
// This function finalizes the key or returns Scared error if recieved message
// is invalid or key derivation fails
func (k *Key) ComputeKey(pake string) ([]byte, error) {
	if k.code == nil {
		return nil, fmt.Errorf("got pake before getting code")
	}

	var pakeMessage PhaseMessage
	phase, err := hex.DecodeString(pake)
	if err != nil {
		return nil, fmt.Errorf("failed to decode the pake message to bytes: %s", err)
	}

	if err := json.Unmarshal(phase, &pakeMessage); err != nil {
		return nil, Scared{kind: invalidPake, err: err}
	}

	p1, err := hex.DecodeString(pakeMessage.Pakev1)
	if err != nil {
		return nil, fmt.Errorf("failed to encode pake string to bytes: %s", err)
	}

	key, err := k.spakeState.Finish(p1)
	if err != nil {
		return nil, Scared{kind: keyDerivationFail, err: err}
	}

	return key, nil
}

// PrepareVersionMessage is for creating bodu of version message to be sent to
// other side after derivation of key using SPAKE2
func (k *Key) PrepareVersionMessage(key []byte) ([]byte, error) {
	dataKey := DerivePhaseKey(key, k.side, "version")
	version := peer.Version{k.version}
	text, err := json.Marshal(version)
	if err != nil {
		return nil, err
	}

	encrypted, err := EncryptData(dataKey, text)
	if err != nil {
		return nil, err
	}

	return encrypted, nil
}
