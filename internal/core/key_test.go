package core

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"math/rand"
	"testing"
	"time"
)

func TestEncryptDecrypt(t *testing.T) {
	rng := rand.New(rand.NewSource(time.Now().UnixNano()))

	key := make([]byte, 32)
	n, err := rng.Read(key)
	if err != nil {
		t.Fatal("Failed to generate a key: ", err)
	}

	if n != 32 {
		t.Fatal("key size is not same as expected: ", n)
	}

	encrypt, err := EncryptData(key, []byte("hello world"))
	if err != nil {
		t.Fatal("Failed to encrypt data: ", err)
	}

	decrypt, err := DecryptData(key, encrypt)
	if err != nil {
		t.Fatal("Failed to decrypt data: ", err)
	}

	if !bytes.Equal(decrypt, []byte("hello world")) {
		t.Fatal("Decrypted data not same as encrypted")
	}
}

func TestEncryptDecryptFail(t *testing.T) {
	rng := rand.New(rand.NewSource(time.Now().UnixNano()))

	key1 := make([]byte, 33)
	rng.Read(key1)

	encrypt, err := EncryptData(key1, []byte("hello world"))
	if encrypt != nil && err == nil {
		t.Fatal("Was expecting error but got encrypted data")
	}

	decrypt, err := DecryptData(key1, []byte(""))
	if decrypt != nil && err == nil {
		t.Fatal("Was expecting error but got decrypted output")
	}

	key2 := make([]byte, 32)
	rng.Read(key2)

	encrypt, _ = EncryptData(key2, []byte("hello world"))
	_, err = DecryptData(key2, encrypt[1:])
	if err == nil {
		t.Fatal("Was expecting decrypt to fail")
	}
}

func TestDerivePhaseKey(t *testing.T) {
	// Compatibility test to see we are doing same thing as python code..
	k := []byte("X\xbf\x9c\x1a\xa3\x9e4\x91\xa6n>)U'A\xa2\x19\xb8^\xd1\x91\xba\xdf\xba\x90\xa1\x1cl\xca\x02D\xd3")
	key := DerivePhaseKey(k, "a242", "version")
	expectedKey := "21863a2400337281463169a6cf64fe5ab9cae64f07aad4a530f942846456ac8b"

	if fmt.Sprintf("%x", key) != expectedKey {
		t.Fatalf("Derived key is not same as execpted: %x", key)
	}
}

func TestBuildPakeComputeKey(t *testing.T) {
	k1 := NewKey("lothar.com/wormhole/text-or-file-xfer", "sideA", make(map[string]string))
	k2 := NewKey("lothar.com/wormhole/text-or-file-xfer", "sideB", make(map[string]string))

	p1, err := k1.BuildPake("4-purple-sausage")
	if err != nil {
		t.Fatal("Build pake failed for side 1: ", err)
	}

	p2, err := k2.BuildPake("4-purple-sausage")
	if err != nil {
		t.Fatal("Build pake failed for side 2: ", err)
	}

	sk1, err := k1.ComputeKey(hex.EncodeToString(p2))
	if err != nil {
		t.Fatal("Compute key failed for side 1: ", err, hex.EncodeToString(p2))
	}

	sk2, err := k2.ComputeKey(hex.EncodeToString(p1))
	if err != nil {
		t.Fatal("Compute key failed for side 2: ", err, hex.EncodeToString(p1))
	}

	if !bytes.Equal(sk1, sk2) {
		t.Fatal("Expected to compute same key from both side")
	}

	v1, err := k1.PrepareVersionMessage(sk1)
	if err != nil {
		t.Fatal("Failed to compute version message 1: ", err)
	}

	v2, err := k2.PrepareVersionMessage(sk2)
	if err != nil {
		t.Fatal("Failed to compute version message 2: ", err)
	}

	vk1 := DerivePhaseKey(sk1, "sideA", "version")
	vk2 := DerivePhaseKey(sk2, "sideB", "version")

	_, err = DecryptData(vk2, v2)
	if err != nil {
		t.Fatal("Decryption failed of v2: ", err)
	}

	_, err = DecryptData(vk1, v1)
	if err != nil {
		t.Fatal("Decryption failed of v1: ", err)
	}
}
