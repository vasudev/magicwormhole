package pgpwords

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"io/ioutil"
	"strconv"
	"strings"
)

var words [][]string

func init() {
	content, err := ioutil.ReadFile("pgpwords.json")
	if err != nil {
		// If we don't have word list then no point in continuing.
		panic(err)
	}

	words = make([][]string, 2)
	evenwords := make([]string, 256)
	oddwords := make([]string, 256)

	stream := bytes.NewReader(content)
	decoder := json.NewDecoder(stream)
	m := make(map[string][]string)
	for decoder.More() {
		err := decoder.Decode(&m)
		if err != nil {
			panic(err)
		}
	}

	for key, values := range m {
		if len(key) == 0 {
			continue
		}
		index, err := strconv.ParseInt(key, 16, 16)
		if err != nil {
			panic(err)
		}

		evenwords[index] = values[1]
		oddwords[index] = values[0]
	}

	words[0] = evenwords
	words[1] = oddwords
}

// ChooseWords selects numwords words and joins them with "-" and returns
// resulting string. If any error is encountered  while reading random value
// function returns empty string with error value set to error occured in
// rand.Read.
func ChooseWords(numwords int) (string, error) {
	var list []string
	b := make([]byte, 1)

	for i := 0; i < numwords; i++ {
		if i%2 == 0 {
			_, err := rand.Read(b)
			if err != nil {
				return "", err
			}
			list = append(list, words[0][int(b[0])])
		} else {
			_, err := rand.Read(b)
			if err != nil {
				return "", err
			}
			list = append(list, words[1][int(b[0])])
		}
	}

	return strings.Join(list, "-"), nil
}

// Complete returns possible completions for a given prefix string, numwords is
// used to append a "-" if more words are expected.
func Complete(prefix string, numwords int) []string {
	var completions []string
	dashes := strings.Count(prefix, "-")

	wordlist := words[dashes%len(words)]
	prefixWords := strings.Split(prefix, "-")

	lastpartialWord := prefixWords[len(prefixWords)-1]
	lastWordLen := len(lastpartialWord)

	for _, word := range wordlist {
		if strings.HasPrefix(word, prefix) {
			if lastWordLen == 0 {

			} else {

			}
		}
	}

}
