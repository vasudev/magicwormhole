package pgpwords

import (
	"strings"
	"testing"
	"testing/quick"
)

func TestChooseWords(t *testing.T) {
	f := func(n uint16) bool {
		w, err := ChooseWords(int(n))
		if err != nil {
			return false
		}

		return strings.Count(w, "-") == int(n-1)
	}

	c := quick.Config{MaxCount: 5}
	if err := quick.Check(f, &c); err != nil {
		t.Error(err)
	}
}
