package peermessages

import (
	"encoding/json"
	"fmt"
	"strings"
)

const (
	message    = "message"
	file       = "file"
	directory  = "directory"
	messageAck = "message_ack"
	fileAck    = "file_ack"
)

var (
	_ Offers = OfferMessage{}
	_ Offers = File{}
	_ Offers = Directory{}

	_ Answers = MessageAck{}
	_ Answers = FileAck{}
)

// Offer represents the offer message received from other peer
type Offer struct {
	Offer Offers `json:"offer"`
}

// UnmarshalJSON implements Unamrshaler interface for Offer type
func (o *Offer) UnmarshalJSON(b []byte) error {
	m := string(b)
	type Alias Offer
	if strings.Contains(m, `"message":`) {
		var aux struct {
			Offer OfferMessage `json:"offer"`
		}
		if err := json.Unmarshal(b, &aux); err != nil {
			return err
		}
		o.Offer = aux.Offer
	} else if strings.Contains(m, `"file":`) {
		var aux struct {
			Offer File `json:"offer"`
		}
		if err := json.Unmarshal(b, &aux); err != nil {
			return err
		}
		o.Offer = aux.Offer
	} else if strings.Contains(m, `"directory":`) {
		var aux struct {
			Offer Directory `json:"offer"`
		}
		if err := json.Unmarshal(b, &aux); err != nil {
			return err
		}
		o.Offer = aux.Offer
	} else {
		return fmt.Errorf("invalid of offer message: %s ", string(b))
	}

	return nil
}

// Offers interface is a way to make sure all offer messages can be represented
// using single type,
type Offers interface {
	OfferType() string
}

// OfferMessage represents offer message which is used to send textual input to
// other side
type OfferMessage struct {
	Message string `json:"message"`
}

// File represents file message received from peer which is intending to send a
// file.
type File struct {
	File FileMessage `json:"file"`
}

// FileMessage represents message contents of a file message received from peer.
type FileMessage struct {
	Name string `json:"filename"`
	Size uint32 `json:"filesize"`
}

// Directory represents message received from peer which intending to send a
// directory.
type Directory struct {
	Directory DirectoryMessage `json:"directory"`
}

// DirectoryMessage represents message contents of directory message from peer.
type DirectoryMessage struct {
	Name    string `json:"dirname"`
	Mode    string `json:"mode"`
	Zipsize uint32 `json:"zipsize"`
	Bytes   uint32 `json:"numbytes"`
	Files   uint32 `json:"numfiles"`
}

// OfferType implements Offers interface for OfferMessage
func (o OfferMessage) OfferType() string {
	return message
}

// OfferType implements Offers interface for FileMessage
func (f File) OfferType() string {
	return file
}

// OfferType implements Offers interface for DirectoryMessage
func (d Directory) OfferType() string {
	return directory
}

// Answer represents message which is sent as acknowledgement for Offer message
// from other peer.
type Answer struct {
	Answer Answers `json:"answer"`
}

// MarshalJSON implements Marshaler interface for Answer type
func (a *Answer) MarshalJSON() (b []byte, err error) {
	switch a.Answer.(type) {
	case MessageAck:
		b, err = json.Marshal(&struct {
			Answer MessageAck `json:"answer"`
		}{
			Answer: a.Answer.(MessageAck),
		})
	case FileAck:
		b, err = json.Marshal(&struct {
			Answer FileAck `json:"answer"`
		}{
			a.Answer.(FileAck),
		})
	}

	return
}

// UnmarshalJSON implements Unmarshaler interface for Answer type
func (a *Answer) UnmarshalJSON(b []byte) error {
	m := string(b)
	if strings.Contains(m, `"message_ack"`) {
		var aux struct {
			Answer MessageAck `json:"answer"`
		}
		if err := json.Unmarshal(b, &aux); err != nil {
			return err
		}
		a.Answer = aux.Answer
	} else if strings.Contains(m, `"file_ack"`) {
		var aux struct {
			Answer FileAck `json:"answer"`
		}
		if err := json.Unmarshal(b, &aux); err != nil {
			return err
		}

		a.Answer = aux.Answer
	} else {
		return fmt.Errorf("invalid answer type: %s", m)
	}

	return nil
}

// Answers is interface to represent possible answer messages that can be sent
// to other peer
type Answers interface {
	AnswerType() string
}

// MessageAck represents acknowledgement message sent to peer after receiving
// and succesfully decryptying the OfferMessage from peer.
type MessageAck struct {
	Ack string `json:"message_ack"`
}

// FileAck represents acknowledgement message sent to peer after receiving and
// succesfully decryptying the FileMessage or DirectoryMessage from peer
type FileAck struct {
	Ack string `json:"file_ack"`
}

// AnswerType implements Answers interface for MessageAck
func (m MessageAck) AnswerType() string {
	return messageAck
}

// AnswerType implements Answers interface for FileAck
func (f FileAck) AnswerType() string {
	return fileAck
}

// Version represents the version message exchanged to do the key confirmation
// from other side
type Version struct {
	AppVersion map[string]string `json:"app_versions"`
}
