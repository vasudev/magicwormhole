package peermessages

import (
	"encoding/json"
	"testing"
)

func TestOfferMessage(t *testing.T) {
	offer := Offer{OfferMessage{"hello world"}}

	// Simply for coverage purpose
	offer.Offer.OfferType()

	offerS, err := json.Marshal(&offer)
	if err != nil {
		t.Fatal("Failed marshaling offer message: ", err)
	}

	expected := `{"offer":{"message":"hello world"}}`
	if string(offerS) != expected {
		t.Fatalf("Marshalled value is not same as expected: %s\n", string(offerS))
	}

	var exOf Offer
	if err = json.Unmarshal(offerS, &exOf); err != nil {
		t.Fatal("Failed to unmarshal the marshaled data: ", err)
	}

	if exOf.Offer.(OfferMessage).Message != offer.Offer.(OfferMessage).Message {
		t.Fatal("Marsahled and unmarshaled values does not match")
	}
}

func TestFileMessage(t *testing.T) {
	file := Offer{File{FileMessage{"foo.txt", 100}}}

	// Simply for coverage purpose
	file.Offer.OfferType()

	fileS, err := json.Marshal(&file)
	if err != nil {
		t.Fatal("Failed marshaling file message: ", err)
	}

	expected := `{"offer":{"file":{"filename":"foo.txt","filesize":100}}}`
	if string(fileS) != expected {
		t.Fatal("Marsahled value not as expected: ", string(fileS))
	}

	var exOf Offer
	if err = json.Unmarshal(fileS, &exOf); err != nil {
		t.Fatal("Failed to unmarshal back marshaled data: ", err)
	}

	if exOf.Offer.(File).File.Name != file.Offer.(File).File.Name {
		t.Fatal("Unmarshaled value not same as marshaled:(Name)")
	}

	if exOf.Offer.(File).File.Size != file.Offer.(File).File.Size {
		t.Fatal("Unmarshaled value not same as marsahled:(Size)")
	}
}

func TestDirectoryMessage(t *testing.T) {
	directory := Offer{Directory{DirectoryMessage{"foo", "0755", 10, 100, 2}}}

	// Simply for coverage purpose
	directory.Offer.OfferType()

	dirS, err := json.Marshal(&directory)
	if err != nil {
		t.Fatal("Failed to marshal directory: ", err)
	}

	expected := `{"offer":{"directory":{"dirname":"foo","mode":"0755","zipsize":10,"numbytes":100,"numfiles":2}}}`
	if string(dirS) != expected {
		t.Fatal("Marshaled value not same as expected: ", string(dirS))
	}

	var exOf Offer
	if err = json.Unmarshal(dirS, &exOf); err != nil {
		t.Fatal("Faile to unmarshal back marshaed data: ", err)
	}

	if exOf.Offer.(Directory).Directory.Name != directory.Offer.(Directory).Directory.Name {
		t.Fatal("Unmarshaled value is not same as marshaled:(dirname)")
	}

	if exOf.Offer.(Directory).Directory.Mode != directory.Offer.(Directory).Directory.Mode {
		t.Fatal("Unmarshaled value is not same as marshaled:(mode)")
	}

	if exOf.Offer.(Directory).Directory.Zipsize != directory.Offer.(Directory).Directory.Zipsize {
		t.Fatal("Unmarshaled value is not same as marshaled:(zipsize)")
	}

	if exOf.Offer.(Directory).Directory.Bytes != directory.Offer.(Directory).Directory.Bytes {
		t.Fatal("Unmarshaled value is not same as marshaled:(numbytes)")
	}

	if exOf.Offer.(Directory).Directory.Files != directory.Offer.(Directory).Directory.Files {
		t.Fatal("Unmarshaled value is not same as marshaled:(numfiles)")
	}

}

func TestMessageAck(t *testing.T) {
	msgack := Answer{MessageAck{"ok"}}

	// simply for coverage
	msgack.Answer.AnswerType()

	msgS, err := json.Marshal(&msgack)
	if err != nil {
		t.Fatal("failed to marshal message_ack", err)
	}

	expected := `{"answer":{"message_ack":"ok"}}`
	if string(msgS) != expected {
		t.Fatal("marshaled message ack is not same as expected: ", string(msgS))
	}

	var exAn Answer
	if err = json.Unmarshal(msgS, &exAn); err != nil {
		t.Fatal("failed to unmarshal marshaled data: ", err)
	}

	if exAn.Answer.(MessageAck).Ack != msgack.Answer.(MessageAck).Ack {
		t.Fatal("unmarshaled data is not same as original: (message_ack)")
	}
}

func TestFileAck(t *testing.T) {
	fileack := Answer{FileAck{"ok"}}

	// simply for coverage
	fileack.Answer.AnswerType()

	fileS, err := json.Marshal(&fileack)
	if err != nil {
		t.Fatal("failed to marshal file_ack ", err)
	}

	expected := `{"answer":{"file_ack":"ok"}}`
	if string(fileS) != expected {
		t.Fatal("Marshaled data is not same as expected: ", string(fileS))
	}

	var exAn Answer
	if err = json.Unmarshal(fileS, &exAn); err != nil {
		t.Fatal("Unmarshaling marshaled data failed: ", err)
	}

	if exAn.Answer.(FileAck).Ack != fileack.Answer.(FileAck).Ack {
		t.Fatal("unmarshaled data is not same as marshaled data (file_ack)")
	}
}

func TestVersion(t *testing.T) {
	expected := `{"app_versions":{}}`
	var version Version
	if err := json.Unmarshal([]byte(expected), &version); err != nil {
		t.Fatal("Failed to unmarshal version message: ", err)
	}

	vB, err := json.Marshal(&version)
	if err != nil {
		t.Fatal("Failed to marshal version information: ", err)
	}

	if string(vB) != expected {
		t.Fatal("Marshaled data does not match original data")
	}
}

func TestInvalidOffers(t *testing.T) {
	invalidMsg := `{"offer":{"message": {"message":"ok"}}}`
	var offer Offer
	if err := json.Unmarshal([]byte(invalidMsg), &offer); err == nil {
		t.Fatal("Should fail to unmarshal invalid offer message: ")
	}

	invalidFile := `{"offer": {"file": "filename", "filesize":"100"}}`
	if err := json.Unmarshal([]byte(invalidFile), &offer); err == nil {
		t.Fatal("should fail to unmarshal invalid file message")
	}

	invalidDirectory := `{"offer": {"directory":"dirname"}}`
	if err := json.Unmarshal([]byte(invalidDirectory), &offer); err == nil {
		t.Fatal("should fail to unmarshal invalid directory message")
	}

	invalidOffer := `{"offer":"ok"}`
	if err := json.Unmarshal([]byte(invalidOffer), &offer); err == nil {
		t.Fatal("should fail on invalid offer message")
	}
}

func TestInvalidAnswers(t *testing.T) {
	invalidMsgAck := `{"answer":"message_ack"}`
	var answer Answer
	if err := json.Unmarshal([]byte(invalidMsgAck), &answer); err == nil {
		t.Fatal("should fail to unmarshal invalid message_ack")
	}

	invalidFileAck := `{"answer":"file_ack"}`
	if err := json.Unmarshal([]byte(invalidFileAck), &answer); err == nil {
		t.Fatal("should fail to unmarshal invalid file_ack")
	}

	invalidAnswer := `{"offer":"ok"}`
	if err := json.Unmarshal([]byte(invalidAnswer), &answer); err == nil {
		t.Fatal("should fail to unmarshal invalid answer type")
	}
}
