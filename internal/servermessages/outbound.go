package servermessages

import (
	"encoding/json"
)

// Bind represents bind message sent to rendezvous server
type Bind struct {
	AppID string `json:"appid"`
	Side  string `json:"side"`
}

// Allocate represents allocate message sent to rendezvous server to allocate
// nameplate.
type Allocate struct{}

// Claim represents claim message sent to rendezvous server for claiming a
// nameplate.
type Claim struct {
	Nameplate string `json:"nameplate"`
}

// Release represents release message sent to rendezvous server to release a
// nameplate.
type Release struct {
	Nameplate string `json:"nameplate"`
}

// Open represents open message sent to rendezvous to open the mailbox alloted
// to the client
type Open struct {
	Mailbox string `json:"mailbox"`
}

// List represents the list message sent to rendezvous server to receive active
// nameplates
type List struct{}

// Add represents the add message used to send message to mailbox on server
type Add struct {
	Phase string `json:"phase"`
	Body  string `json:"body"`
}

// Close represents close message sent to server to close mailbox and mention
// mood of the client
type Close struct {
	Mailbox string `json:"mailbox"`
	Mood    string `json:"mood"`
}

// MarshalJSON implements Marshaler interface for  Bind type
func (b Bind) MarshalJSON() ([]byte, error) {
	type Alias Bind
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  "bind",
		Alias: (*Alias)(&b),
	})
}

// OutType is just interface to make outgoing messages of type Bind as
// OutgoingMessage type
func (b *Bind) OutType() string {
	return bind
}

// MarshalJSON implements Marshaler interface for Allocate type
func (a Allocate) MarshalJSON() ([]byte, error) {
	return []byte(`{"type": "allocate"}`), nil
}

// OutType is just interface to make outgoing messages of type Allocate as
// OutgoingMessage type
func (a Allocate) OutType() string {
	return allocate
}

// MarshalJSON implements Marshaler interface for Claim type
func (c Claim) MarshalJSON() ([]byte, error) {
	type Alias Claim
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  "claim",
		Alias: (*Alias)(&c),
	})
}

// OutType is just interface to make outgoing messages of type Claim as
// OutgoingMessage type
func (c Claim) OutType() string {
	return claim
}

// MarshalJSON implements Marshaler interface for Release type
func (r Release) MarshalJSON() ([]byte, error) {
	type Alias Release
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  "release",
		Alias: (*Alias)(&r),
	})
}

// OutType is just interface to make outgoing messages of type Release as
// OutgoingMessage type
func (r Release) OutType() string {
	return release
}

// MarshalJSON implements Marshaler interface for Open type
func (o Open) MarshalJSON() ([]byte, error) {
	type Alias Open
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  "open",
		Alias: (*Alias)(&o),
	})
}

// OutType is just interface to make outgoing messages of type Open as
// OutgoingMessage type
func (o Open) OutType() string {
	return open
}

// MarshalJSON implements Marshaler interface for List type
func (l List) MarshalJSON() ([]byte, error) {
	type Alias List
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  list,
		Alias: (*Alias)(&l),
	})
}

// OutType is an OutgoingMessage interface implementation
func (l List) OutType() string {
	return list
}

// MarshalJSON implements Marshaler interface for Add type
func (a Add) MarshalJSON() ([]byte, error) {
	type Alias Add
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  add,
		Alias: (*Alias)(&a),
	})
}

// OutType is an OutgoingMessage interface implementation
func (a Add) OutType() string {
	return add
}

// MarshalJSON implements Marshaler interface for Close.
func (c Close) MarshalJSON() ([]byte, error) {
	type Alias Close
	return json.Marshal(&struct {
		Type string `json:"type"`
		*Alias
	}{
		Type:  close,
		Alias: (*Alias)(&c),
	})
}

// OutType implementation for OutgoingMessage interface.
func (c Close) OutType() string {
	return close
}
