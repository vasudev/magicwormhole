package servermessages

import (
	"encoding/json"
	"testing"
)

func TestDecodeWelcome(t *testing.T) {
	welcome1Msg := `{"type": "welcome", "welcome": {"motd":"hello world"}}`
	welcome2Msg := `{"type": "welcome", "welcome": {}}`

	w1, err := Decode(json.RawMessage(welcome1Msg))
	if err != nil {
		t.Errorf("Failed to decode message: %s with error: %v", welcome1Msg, err)
	}

	if w1.InType() != welcome {
		t.Error("Unknown welcome message?", w1.InType())
	} else {
		w := w1.(*Welcome)
		if _, ok := w.Welcome["motd"]; !ok {
			t.Error("Welcome message did not have motd key")
		}
	}

	w2, err := Decode(json.RawMessage(welcome2Msg))
	if err != nil {
		t.Errorf("Failed to decode message: %s with error: %v", welcome2Msg, err)
	}

	if w2.InType() != welcome {
		t.Error("Unknown welcome message?", w1.InType())
	} else {
		w := w2.(*Welcome)
		if len(w.Welcome) != 0 {
			t.Error("Not expecting any key values in welcome message")
		}
	}
}

func TestDecodeNameplates(t *testing.T) {
	nameplates1 := `{"type": "nameplates", "nameplates": []}`
	nameplates2 := `{"type": "nameplates", "nameplates": [{"id":"1"}, {"id": "2"}, {"id":"4"}]}`

	n1, err := Decode(json.RawMessage(nameplates1))
	if err != nil {
		t.Errorf("Failed to decode nameplates message: %v", err)
	}

	if n1.InType() != nameplates {
		t.Errorf("Expected nameplates type but found: %s", n1.InType())
	} else {
		n := n1.(*Nameplates)
		if len(n.Nameplates) != 0 {
			t.Errorf("Was expecting empty nameplates list: but found %v", n.Nameplates)
		}
	}

	n2, err := Decode(json.RawMessage(nameplates2))
	if err != nil {
		t.Errorf("Failed to decode nameplates: %v", err)
	}

	if n2.InType() != nameplates {
		t.Errorf("Was expecting nameplates message but found: %s", n2.InType())
	} else {
		n := n2.(*Nameplates)
		if len(n.Nameplates) == 0 {
			t.Error("Was expecting name plates list but found none")
		}
	}
}

func TestDecodeAllocated(t *testing.T) {
	allocated1 := `{"type": "allocated", "nameplate": "1"}`
	a1, err := Decode(json.RawMessage(allocated1))
	if err != nil {
		t.Errorf("Failed to decode allocated: %v", err)
	}

	if a1.InType() != allocated {
		t.Errorf("Was expecting allocated message but found: %s", a1.InType())
	} else {
		a := a1.(*Allocated)
		if *a.Nameplate != "1" {
			t.Errorf("Was expecting nameplate 1 but found %s", *a.Nameplate)
		}
	}

}

func TestDecodeClaimed(t *testing.T) {
	claimed1 := `{"type": "claimed", "mailbox":"axhfda"}`
	claimed2 := `{"type": "claimed"}`

	c1, err := Decode(json.RawMessage(claimed1))
	if err != nil {
		t.Errorf("Failed to decode claimed message: %v", err)
	}

	if c1.InType() != claimed {
		t.Errorf("Expecting claimed message but found: %s", c1.InType())
	} else {
		c := c1.(*Claimed)
		if *c.Mailbox != "axhfda" {
			t.Errorf("Expecting mailbox axhfda but found: %s", *c.Mailbox)
		}
	}

	c2, err := Decode(json.RawMessage(claimed2))
	if err != nil {
		t.Errorf("Failed to decode claimed message: %v", err)
	}

	c := c2.(*Claimed)
	if c.Mailbox != nil {
		t.Errorf("Was expecting nil mailbox but found: %s", *c.Mailbox)
	}
}

func TestDecodeMessage(t *testing.T) {
	message1 := `{"body": "7b2270616b655f7631223a22353361346566366234363434303364376534633439343832663964373236646538396462366631336632613832313537613335646562393562366237633536353533227d", "server_rx": 1523468188.293486, "id": null, "phase": "pake", "server_tx": 1523498654.753594, "type": "message", "side": "side1"}`
	m1, err := Decode(json.RawMessage(message1))
	if err != nil {
		t.Errorf("Failed to decode Message: %v", err)
	}

	if m1.InType() != message {
		t.Errorf("Expecting Message message but found: %s", m1.InType())
	}

	m := m1.(*Message)
	if m.Body == nil {
		t.Error("Was expecting message body but found nil")
	}

	if m.Phase == nil {
		t.Error("Was expecting message phase but found nil")
	}

	if m.Side == nil {
		t.Error("Was expecting message side but found nil")
	}

	if *m.Phase != "pake" {
		t.Errorf("Was expecting a pake message but found: %s", *m.Phase)
	}

	if *m.Side != "side1" {
		t.Errorf("Was expecting a side1 message but found: %s", *m.Side)
	}

}

func TestDecodeError(t *testing.T) {
	message := `{"error":"bind failed","orig":{"type":"bind","appid":"lothar.com/wormhole/text-file-xfer","side":"ab3c3d"}}`
	m1, err := Decode(json.RawMessage(message))
	if err != nil {
		t.Errorf("Failed to decode message: %s", err)
	}

	if m1.InType() != werror {
		t.Errorf("Expecting error message but found: %s", m1.InType())
	}

	w := m1.(*WError)
	switch w.Orig.(type) {
	case *Bind:
		// We are good nothing to do
	default:
		t.Errorf("Was expecting type bind but found: %t", w.Orig)
	}

	message2 := `{"error":"was expecting bind message", "orig":{"type":"allocate"}}`
	m2, err := Decode(json.RawMessage(message2))
	if err != nil {
		t.Errorf("Failed to decode message: %s", err)
	}

	w = m2.(*WError)
	switch w.Orig.(type) {
	case *Allocate:
		// We are good nothing to do
	default:
		t.Errorf("Expecting allocate but got: %t", w.Orig)
	}

}
