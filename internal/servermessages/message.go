package servermessages

import (
	"encoding/json"
	"strings"
)

// InboundMessage interface should to be followed by all incoming messges from server.
type InboundMessage interface {
	InType() string
}

// OutboundMessage interface should be followed by all outgoing messages from client.
type OutboundMessage interface {
	OutType() string
}

const (
	// Outbound messages i.e C->S
	bind     = "bind"
	list     = "list"
	allocate = "allocate"
	claim    = "claim"
	release  = "release"
	open     = "open"
	add      = "add"
	close    = "close"
	ping     = "ping"

	// Inbound messages i.e. S->C
	welcome    = "welcome"
	nameplates = "nameplates"
	allocated  = "allocated"
	claimed    = "claimed"
	released   = "released"
	message    = "message"
	closed     = "closed"
	ack        = "ack"
	pong       = "pong"
	werror     = "error"
)

// Decode decodes the incoming raw json message, and returns InboundMessage or
// error if decoding fails.
func Decode(msg json.RawMessage) (InboundMessage, error) {
	msgString := string(msg)
	if strings.Contains(msgString, `"error":`) ||
		strings.Contains(msgString, `"error" :`) {
		var errorData struct {
			Error string `json:"error"`
		}

		if err := json.Unmarshal(msg, &errorData); err != nil {
			return nil, err
		}

		var origMessage struct {
			Orig struct {
				Type string `json:"type"`
			} `json:"orig"`
		}

		if err := json.Unmarshal(msg, &origMessage); err != nil {
			return nil, err
		}

		var o OutboundMessage
		switch origMessage.Orig.Type {
		case bind:
			o = &Bind{}
		case allocate:
			o = &Allocate{}
		case claim:
			o = &Claim{}
		case release:
			o = &Release{}
		case open:
			o = &Open{}
		case list:
			o = &List{}
		case add:
			o = &Add{}
		case close:
			o = &Close{}
		}

		if err := json.Unmarshal(msg, o); err != nil {
			return nil, err
		}
		return &WError{errorData.Error, o}, nil

	}

	var typeData struct {
		Type string `json:"type"`
	}

	if err := json.Unmarshal(msg, &typeData); err != nil {
		return nil, err
	}

	var m InboundMessage
	switch typeData.Type {
	case welcome:
		m = &Welcome{}
	case nameplates:
		m = &Nameplates{}
	case allocated:
		m = &Allocated{}
	case claimed:
		m = &Claimed{}
	case released:
		m = &Released{}
	case message:
		m = &Message{}
	case closed:
		m = &Closed{}
	case ack:
		m = &Ack{}
	case pong:
		m = &Pong{}
	}

	if err := json.Unmarshal(msg, m); err != nil {
		return nil, err
	}

	return m, nil
}
