package servermessages

// Ensure all the inbound messages implement IncommingMessage interface
var (
	_ InboundMessage = (*Welcome)(nil)
	_ InboundMessage = (*Nameplates)(nil)
	_ InboundMessage = (*Allocated)(nil)
	_ InboundMessage = (*Claimed)(nil)
	_ InboundMessage = (*Message)(nil)
	_ InboundMessage = (*Pong)(nil)
	_ InboundMessage = (*Released)(nil)
	_ InboundMessage = (*Closed)(nil)
	_ InboundMessage = (*Ack)(nil)
)

// Welcome represents welcome message sent by server on websocket connection
// from client
type Welcome struct {
	Welcome map[string]string `json:"welcome"`
}

// InType implementation to make Welcome as compatible IncomingMessage type.
// type.
func (w *Welcome) InType() string {
	return welcome
}

// Nameplate represents individual nameplate got from server which is dictionary
// with key id and value as nameplate.
type Nameplate struct {
	ID string `json:"id"`
}

// Nameplates represents nameplate message from server which is sent when client
// request active nameplates on server.
type Nameplates struct {
	Nameplates []Nameplate `json:"nameplates"`
}

// InType implementation to make Nameplates as compatible IncomingMessage type.
func (n *Nameplates) InType() string {
	return nameplates
}

// Allocated represents allocated message from server which provides the
// nameplate to client.
type Allocated struct {
	Nameplate *string `json:"nameplate"`
}

// InType implementation to make Allocated as compatible IncomingMessage type.
func (a *Allocated) InType() string {
	return allocated
}

// Claimed represents claimed message from server which provides mailbox
// reserved by the server for the session.
type Claimed struct {
	Mailbox *string `json:"mailbox"`
}

// InType implementation to make Claimed as compatible IncomingMessage type
func (c *Claimed) InType() string {
	return claimed
}

// Message represents "message" type message from the server.
type Message struct {
	Side  *string `json:"side"`
	Phase *string `json:"phase"`
	Body  *string `json:"body"`
}

// InType implementation to make Message type as compatible IncomingMessage
// type.
func (m *Message) InType() string {
	return message
}

// Pong represents pong response from server
// TODO: do we really need this?
type Pong struct {
	Pong *uint32 `json:"pong"`
}

// InType implementation to make Pong as compatible IncomingMessage
func (p *Pong) InType() string {
	return pong
}

// Released represents released message from server received on releasing a
// nameplate.
type Released struct{}

// InType implementation to make Released as compatible IncomingMessage
func (r *Released) InType() string {
	return released
}

// Closed represents closed message received from server
type Closed struct{}

// InType implementation to make Closed as compatible IncomingMessage
func (c *Closed) InType() string {
	return closed
}

// Ack represents Ack message received from server.
type Ack struct{}

// InType implementation to make Ack as compatible IncomingMessage
func (c *Ack) InType() string {
	return ack
}

// WError represents error message sent by server when there is a error done by
// client.
type WError struct {
	Msg  string          `json:"error"`
	Orig OutboundMessage `json:"orig"`
}

// InType implementation to make WError as compatible InboundMessage
func (w *WError) InType() string {
	return werror
}
