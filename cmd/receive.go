package main

import (
	"encoding/hex"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/url"
	core "salsa.debian.org/vasudev/magicwormhole/internal/core"
	peer "salsa.debian.org/vasudev/magicwormhole/internal/peermessages"
	messages "salsa.debian.org/vasudev/magicwormhole/internal/servermessages"
	"strconv"
	"time"
)

func start(incoming chan messages.InboundMessage, c *websocket.Conn) {
	var k core.Key
	var sk []byte
	var phase int
	var nameplate, mailbox string
	nameplateReleased := false
	for {
		select {
		case msg := <-incoming:
			switch msg.(type) {
			case *messages.Welcome:
				w := msg.(*messages.Welcome)
				if motd, ok := w.Welcome["motd"]; ok {
					log.Println("Motd: ", motd)
				}

				if err := c.WriteJSON(
					messages.Bind{"lothar.com/wormhole/text-or-file-xfer", "side0"}); err != nil {
					log.Fatal("Failed to send bind message: ", err)
				}

				if err := c.WriteJSON(messages.List{}); err != nil {
					log.Fatal("Failed to send list message: ", err)
				}

			case *messages.Allocated:
				a := msg.(*messages.Allocated)
				if err := c.WriteJSON(messages.Claim{*a.Nameplate}); err != nil {
					log.Fatal("Failed to claim nameplate: ", err)
				}
			case *messages.Claimed:
				claimed := msg.(*messages.Claimed)
				if err := c.WriteJSON(messages.Open{*claimed.Mailbox}); err != nil {
					log.Fatal("Failed to open mailbox: ", err)
				}
				mailbox = *claimed.Mailbox
			case *messages.Nameplates:
				n := msg.(*messages.Nameplates)
				log.Println("Active nameplate on server are: ")
				for _, ns := range n.Nameplates {
					fmt.Printf("%s, ", ns.ID)
				}
				fmt.Println()
				fmt.Print("Enter nameplate: ")
				fmt.Scanf("%s\n", &nameplate)
				if err := c.WriteJSON(messages.Claim{nameplate}); err != nil {
					log.Fatal("Failed to send claim message: ", err)
				}
			case *messages.Message:
				m := msg.(*messages.Message)
				// Now we can safely release the nameplates
				if !nameplateReleased {
					if err := c.WriteJSON(messages.Release{nameplate}); err != nil {
						log.Fatal("Failed to release the nameplate: ", err)
					}
				}
				switch *m.Phase {
				case "pake":
					if *m.Side != "side0" {
						// TODO create our pake and send and derive key usiing their pake
						var err error
						k = core.NewKey("lothar.com/wormhole/text-or-file-xfer", "side0", make(map[string]string))
						var code string
						fmt.Print("Enter passcode: ")
						fmt.Scanf("%s", &code)
						fmt.Println("Code read: ", code)

						pake, err := k.BuildPake(code)
						if err != nil {
							panic(fmt.Sprintf("Failed to compute pake: %s\n", err))
						}

						sk, err = k.ComputeKey(*m.Body)
						if err != nil {
							panic(fmt.Sprintf("Failed to compute key: %s", err))
						}
						fmt.Printf("sending key: %x\n", sk)

						outMessage := messages.Add{"pake", hex.EncodeToString(pake)}
						if err = c.WriteJSON(outMessage); err != nil {
							log.Fatal("Failed to send pake message: ", err)
						}

					}

				default:
					// TODO derive phase key and decrypt data
					if *m.Side != "side0" {
						dk := core.DerivePhaseKey(sk, *m.Side, *m.Phase)
						bodyBytes, _ := hex.DecodeString(*m.Body)
						data, err := core.DecryptData(dk, bodyBytes)
						if err != nil {
							log.Fatal("decryption failed, ", err)
						}

						fmt.Printf("phase: %s key: %x\n", *m.Phase, dk)
						if *m.Phase == "version" {
							var version peer.Version
							if err := json.Unmarshal(data, &version); err != nil {
								log.Fatal("Failed to unmarshal version message: ", err)
							}
							fmt.Printf("%v\n", version)
							v, err := k.PrepareVersionMessage(sk)
							if err != nil {
								log.Fatal("Failed to create version message")
							}

							if err = c.WriteJSON(messages.Add{"version", hex.EncodeToString(v)}); err != nil {
								log.Fatal("Failed to send version message: ", err)
							}

						} else {
							var offer peer.Offer
							if err := json.Unmarshal(data, &offer); err != nil {
								log.Fatal("json unmarshal of offer failed, ", err)
							}
							switch offer.Offer.(type) {
							case peer.OfferMessage:
								fmt.Printf("Message from other side: %s\n", offer.Offer.(peer.OfferMessage).Message)
							case peer.File:
								fmt.Printf("Peer from other side is offering file: %v\n", offer.Offer.(peer.File))
							case peer.Directory:
								fmt.Printf("Peer from other side is offering a directory: %v\n", offer.Offer.(peer.Directory))
							}

							phase, _ = strconv.Atoi(*m.Phase)
							pk := core.DerivePhaseKey(sk, "side0", fmt.Sprintf("%d", phase+1))

							ack := peer.Answer{peer.MessageAck{"ok"}}
							ackB, err := json.Marshal(ack)
							if err != nil {
								log.Fatal("failed to marshal the ack: ", err)
							}
							encrypted, err := core.EncryptData(pk, ackB)
							if err != nil {
								log.Fatal("failed to encrypt the reply: ", err)
							}

							reply := messages.Add{fmt.Sprintf("%d", phase+1), hex.EncodeToString(encrypted)}
							if err = c.WriteJSON(reply); err != nil {
								log.Fatal("failed to send response to other side: ", err)
							}

							if err = c.WriteJSON(messages.Close{mailbox, "happy"}); err != nil {
								log.Fatal("Failed to close mailbox: ", err)
							}
						}
					}
				}
			case *messages.Released:
				nameplateReleased = true
			case *messages.Closed:
				return

			}
		}
	}
}

func main() {
	u := url.URL{Scheme: "ws", Host: "localhost:4000", Path: "v1"}
	log.Printf("connecting to %s", u.String())

	c, _, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		log.Fatal("dial: ", err)
	}

	defer c.Close()

	input := make(chan messages.InboundMessage)
	inputDone := make(chan bool)

	inputTimer := time.NewTicker(time.Second)
	defer inputTimer.Stop()

	go func() {
		defer close(input)
		var in messages.InboundMessage
		for {
			select {
			case <-inputDone:
				return
			case <-inputTimer.C:
				_, message, err := c.ReadMessage()
				if err != nil {
					log.Println("Read: ", err)
				}

				in, err = messages.Decode(json.RawMessage(message))
				if err != nil {
					log.Fatal("Decode: ", err, string(message))
				}
				input <- in
			}

		}
	}()

	start(input, c)
}
