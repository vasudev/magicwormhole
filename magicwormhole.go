package magicwormhole

import (
	"fmt"
	"salsa.debian.org/vasudev/magicwormhole/internal/core"
	messages "salsa.debian.org/vasudev/magicwormhole/internal/servermessages"
)

// Wormhole represents the magic-wormhole which can be used by clients to do
// secure file/message transfer between them.
type Wormhole struct {
	relay     string
	appid     string
	side      string
	code      *string
	nameplate *string
	mailbox   *string
	session   *core.Session
}

// Create creates a Wormhole with given appid and relayURL parameters. On
// success function returns pointer to newly creaed Wormhole and welcome map
// received from the server.
func Create(appid, relayURL string) (*Wormhole, error) {
	wormhole := new(Wormhole)
	session, err := core.NewSession(relayURL)
	if err != nil {
		return nil, err
	}

	side, err := generateSide()
	if err != nil {
		return nil, err
	}

	wormhole.session = session
	wormhole.relay = relayURL
	wormhole.appid = appid
	wormhole.side = side

	return wormhole, nil
}

// GetWelcome sends bind message to server and receives Welcome message back
// which might contain some information from the server. Clients can use this
// information to display to end-user.
func (w *Wormhole) GetWelcome() (map[string]string, error) {
	w.session.ForServer <- &messages.Bind{w.appid, w.side}
	message := <-w.session.ToApp

	switch message.(type) {
	case *messages.Welcome:
		return message.(*messages.Welcome).Welcome, nil
	case *messages.WError:
		errMsg := message.(*messages.WError)
		return nil, &ServerError{errMsg.Msg, errMsg.Orig}
	default:
		// This case should not happen
		return nil, fmt.Errorf("Got unexpected message from server: %v", message)
	}
}

// AllocateCode allocates new wormhole code which includes nameplate and words
// separated by '-'. length suggests how many words is needed. Allocated
// wormhole code can be received back using GetCode function.
func (w *Wormhole) AllocateCode(length int) {

}
